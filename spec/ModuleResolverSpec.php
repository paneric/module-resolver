<?php

namespace spec\Paneric\ModuleResolver;

use Paneric\ModuleResolver\ModuleResolver;
use PhpSpec\ObjectBehavior;

class ModuleResolverSpec extends ObjectBehavior
{
    private $config = [
        'default_route_key' => 'home',
        'local_map' => ['fr', 'nl', 'de', 'en', 'pl'],
        'module_map' => [
            'error' => 'Error',
            'home' => 'Website',
            'signin' => 'Authentication',
        ],
    ];

    public function it_is_initializable()
    {
        $this->shouldHaveType(ModuleResolver::class);
    }

    public function it_sets_module_folder_name()
    {
        $this->setModuleFolderName('/', $this->config)->shouldReturn('Website/');
        $this->setModuleFolderName('/fr', $this->config)->shouldReturn('Website/');

        $this->setModuleFolderName('/xxxx', $this->config)->shouldReturn('Website/');

        $this->setModuleFolderName('/error', $this->config)->shouldReturn('Error/');
        $this->setModuleFolderName('/error/fr', $this->config)->shouldReturn('Error/');
    }
}
