<?php

return [
    'settings' => [
        'settings_folder' => 'definitions/settings/',
        'application' => [
            'twig',
            'orm',
            'translation',
        ],
        'modules' => [
            'Website' => [
                'libraries',
                'middleware',
                'translation',
            ],
            'Registration' => [
                'validation',
                'translation',
            ],
        ],
    ],
];
