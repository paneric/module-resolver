<?php

declare(strict_types=1);

namespace Paneric\ModuleResolver;

class Local
{
    private ?string $value;

    public function setValue(array $config, ?string $local = null): ?string
    {
        if ($local !== null) {
            setcookie(
                $config['name'],
                $local,
                $config['expire'],
                $config['path'],
                $config['domain'],
                $config['security'],
                $config['http_only']
            );

            $this->value = $local;

            return $this->value;
        }

        if (!isset($_COOKIE[$config['name']])) {

            setcookie(
                $config['name'],
                $config['default_local'],
                $config['expire'],
                $config['path'],
                $config['domain'],
                $config['security'],
                $config['http_only']
            );

            $this->value = $config['default_local'];

            return $this->value;
        }

        $this->value = $_COOKIE[$config['name']];

        return $this->value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}
