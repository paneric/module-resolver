<?php

declare(strict_types=1);

namespace Paneric\ModuleResolver;

use Exception;

class ModularEnvironment
{
    private string $apiUrl;
    private array $processFoldersPaths;
    private string $local;
    private array $definitions;

    /**
     * @throws Exception
     */
    public function build(
        array $moduleResolverConfig,
        array $moduleMap,
        array $localConfig,
        string $env
    ): self {
        $moduleResolver = new ModuleResolver();
        $moduleResolver->processRequest(
            $moduleResolverConfig,
            $moduleMap
        );
        $this->apiUrl = $moduleResolver->getApiUrl();
        $this->processFoldersPaths = $moduleResolver->getProcessFoldersPaths();

        $local = new Local();
        $this->local = $local->setValue(
            $localConfig[$env],
            $moduleResolver->getLocal()
        );

        $definitionsCollector = new DefinitionsCollector();
        $this->definitions = $definitionsCollector->setDefinitions(
            $this->processFoldersPaths,
            'config',
            $localConfig[$env]['default_local'],
            $env
        );

        return $this;
    }

    public function getApiUrl(): string
    {
        return $this->apiUrl;
    }
    public function getProcessFoldersPaths(): array
    {
        return $this->processFoldersPaths;
    }
    public function getLocal(): string
    {
        return $this->local;
    }
    public function getDefinitions(): array
    {
        return $this->definitions;
    }
}
