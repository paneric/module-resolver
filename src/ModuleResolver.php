<?php

declare(strict_types=1);

namespace Paneric\ModuleResolver;

use Exception;

class ModuleResolver
{
    private ?string $requestUri = null;
    private ?string $queryParams = null;
    private ?array $uriSegments = null;
    private ?string $local = null;
    private ?string $apiUrl = null;
    private ?array $processFoldersPaths = null;

    public function setModuleTemplatesFolderName(string $moduleFolderName): string
    {
        $pattern = '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!';
        preg_match_all($pattern, $moduleFolderName, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match === strtoupper($match) ?
                strtolower($match) :
                lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * @throws Exception
     */
    public function processRequest(array $config, array $moduleMap): void
    {
        $this->requestUri = $_SERVER['REQUEST_URI'];

        $this->separateRequestUriFromQueryParams();

        $this->prepareUriSegments($config, $moduleMap);

        $this->extractLocal($config);

        $this->processFoldersPaths = $this->prepareProcessFoldersPaths($config, $moduleMap);
    }

    protected function separateRequestUriFromQueryParams(): void
    {
        $requestUriParts = explode('?', $this->requestUri);
        $this->requestUri = $requestUriParts[0];

        if (isset($requestUriParts[1]) && !empty($requestUriParts[1])) {
            $this->queryParams = $requestUriParts[1];
        }
    }

    protected function prepareUriSegments(array $config, array $moduleMap): void
    {
        $moduleMap = $this->setModuleMap($config, $moduleMap);
        $dashAsSlash = $config['dash_as_slash'] ?? false;

        $this->requestUri = str_replace($config['proxy_prefix'], '', $this->requestUri);

        if ($dashAsSlash && !str_contains($this->requestUri, '/')) {
            $this->requestUri = str_replace('-', '/', $this->requestUri);
        }

        $this->uriSegments = explode('/', $this->requestUri);
    }

    protected function extractLocal(array $config): void
    {
        $localMap = $config['local_map'];

        if (in_array(strtolower(end($this->uriSegments)), $localMap, true)) {
            $this->local = array_pop($this->uriSegments);
            return;
        }

        foreach ($localMap as $local) {
            if (str_contains($this->queryParams, 'local=' . $local)) {
                $this->local = $local;
                return;
            }
        }
    }

    protected function prepareProcessFoldersPaths(array $config, array $moduleMap): array
    {
        $defaultRouteKey = $config['default_route_key'];

        if (empty($this->uriSegments)) {
            return [$moduleMap[$defaultRouteKey] . '/'];
        }

        $moduleKey = strtolower($this->uriSegments[1]);

        if ($moduleKey === '') {
            return [$moduleMap[$defaultRouteKey] . '/'];
        }

        foreach ($moduleMap as $apiUrl => $moduleFolders) {
            if (array_key_exists($moduleKey, $moduleFolders)) {
                $this->apiUrl = $apiUrl;
                return $this->setFoldersPaths($moduleFolders[$moduleKey], $config);
            }
        }

        return [$moduleMap[$defaultRouteKey] . '/'];
    }

    protected function setModuleMap(array $config, array $moduleMap): array
    {
        if (!$config['merge_module_cross_map']) {
            return $moduleMap;
        }

        $moduleMapCross = $config['module_map_cross'];

        foreach ($moduleMapCross as $partialRoute => $crossPath) {
            $moduleMap[$partialRoute] = $crossPath;
        }

        return $moduleMap;
    }

    protected function setFoldersPaths(string $targetFolder, array $config): string|array
    {
        $processFolderPaths = [$config['app_path']];

        $rootPaths = $config['root_paths'];
        foreach ($rootPaths as $rootPath) {
            if (str_contains($targetFolder, $rootPath)) {
                $pathParts = str_replace($rootPath, '', $targetFolder);
                $pathParts = explode('/', $pathParts);
                $processFoldersPaths = $rootPath;

                foreach ($pathParts as $pathPart) {
                    $processFoldersPaths .= $pathPart . '/';
                    $processFolderPaths[] = $processFoldersPaths;
                }
                break;
            }
        }

        return $processFolderPaths;
    }

    public function getLocal(): ?string
    {
        return $this->local;
    }

    public function getProcessFoldersPaths(): ?array
    {
        return $this->processFoldersPaths;
    }

    public function getApiUrl(): ?string
    {
        return $this->apiUrl;
    }
}
