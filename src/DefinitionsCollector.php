<?php

declare(strict_types=1);

namespace Paneric\ModuleResolver;

class DefinitionsCollector
{
    private array $definitions = [];

    private array $envConfigs = ['dev.php', 'test.php', 'prod.php'];

    public function getDefinitions(): array
    {
        return $this->definitions;
    }

    /** @noinspection PhpIncludeInspection */
    public function setDefinitions(
        array $processFoldersPaths,
        string $definitionsFolderName,
        string $local = null,
        string $env = null
    ): array {
        $definitionsFoldersPaths = [];

        foreach ($processFoldersPaths as $path) {
            $definitionsFoldersPaths[] = $path . $definitionsFolderName . '/';
        }

        $definitions = [];
        foreach ($definitionsFoldersPaths as $scopePath) {// app/definitions/, module/definitions
            if (!is_dir($scopePath)) {
                continue;
            }

            $scopeDefinitionsFolderContent = array_diff(scandir($scopePath), ['.', '..']);

            foreach ($scopeDefinitionsFolderContent as $scopeDefinitionsFolderItem) {//settings, di etc
                $definitionFolderPath = $scopePath . $scopeDefinitionsFolderItem . '/';

                if(is_dir($definitionFolderPath)) {
                    $filesNames = array_diff(scandir($definitionFolderPath), ['.', '..']);

                    foreach ($filesNames as $fileName) {
                        if(is_file($definitionFolderPath . $fileName)) {
                            $fileName = $this->checkExtension($fileName);
                            $fileName = $this->checkTranslationSettings($fileName, $local);
                            $fileName = $this->checkEnvironmentSettings($fileName, $env);

                            if ($fileName !== null) {
                                $definitions = array_merge(
                                    $definitions,
                                    (array) require($definitionFolderPath . $fileName)
                                );
                            }
                        }
                    }
                }
            }
        }

        $this->definitions = $definitions;

        return $this->definitions;
    }

    private function checkExtension(string $fileName): ?string
    {
        if (pathinfo($fileName)['extension'] === 'php') {
            return $fileName;
        }

        return null;
    }

    private function checkTranslationSettings(string $fileName = null, string $local = null): ?string
    {
        if ($local === null || $fileName === null) {
            return $fileName;
        }

        if (str_contains($fileName, 'translation')) {
            if(str_contains($fileName, $local)) {
                return $fileName;
            }

            return null;
        }

        return $fileName;
    }

    private function checkEnvironmentSettings(string $fileName = null, string $env = null): ?string
    {
        if ($env === null  || $fileName === null) {
            return $fileName;
        }

        if (in_array($fileName, $this->envConfigs, true)) {
            if ($fileName === $env . '.php') {
                return $fileName;
            }

            return null;
        }

        return $fileName;
    }
}
